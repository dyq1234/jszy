from django.contrib import admin

# Register your models here.
from .models import User
#需要加在admin.site.register(User,UserAdmin) 才会生效
class UserAdmin(admin.ModelAdmin):
    '''
    list_display=['pk','uname','upwd']  显示字段
    list_filter = ['uname'] 过滤字段
    search_fields = ['uname'] 查找字段
    list_per_page = 5   分页 每5条为一页
    '''
    #列表页属性
    list_display = ['pk','uname']
    list_filter = ['uname']
    search_fields = ['uname']
    # list_per_page =

    '''
    fields = ['pwd','uname']  规定属性的先后顺序
    fieldsets = [
    ('num',{'fields':['uname','upwd']})
    ]  给属性分组
    fields 与 fieldsets 不能同时使用
    '''
    #添加修改页属性
    # fields = []
    # fieldsets = []

#注册 如果不注册，站点里面查不到
admin.site.register(User,UserAdmin)