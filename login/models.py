from django.db import models

# Create your models here.
'''
1.每个模型相当于单个数据库表，每个属性也是这个表中的一个字段

2.注意到Django在选择所有数据时并没有使用 SELECT* ，而是显式列出了所有字段。 设计的时候就是这样： SELECT* 会更慢，而且最重要的是列出所有字段遵循了Python 界的一个信条： 明言胜于暗示。

3.在 name 和 contains 之间有双下划线。和Python一样，Django也使用双下划线来表明会进行一些魔术般的 操作。这里，contains部分会被Django翻译成LIKE语句：

4.`` filter()`` 函数返回一个记录集，这个记录集是一个列表。 相对列表来说，有些时候我们更需要 获取单个的对象， `` get()`` 方法就是在此时使用

5.class Meta，内嵌于 Publisher 这个类的定义中（如果 class Publisher 是顶格的，那么 class Meta 在它之下要缩进4个空格－－按 Python 的传统 ）。
你可以在任意一个 模型 类中 使用 Meta 类，来设置一些与特定模型相关的选项。 在 附录B 中有 Meta 中所有可选项的完整参考，现在，我们 关注 ordering 
这个选项就够了。 如果你设置了这个选项，那么除非你检索时特意额外地使用了 order_by()， 否则，当你使用 Django 的数据库 API 去检索时，Publisher对
象的相关返回值默认地都会按 name 字段排序
'''


class User(models.Model):
    uname = models.CharField(max_length=10)
    upwd = models.CharField(max_length=10)
    class Meta:
        db_table='user'

    def __str__(self):
        return self.uname