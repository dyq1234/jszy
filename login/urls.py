from django.conf.urls import url

from django.urls import path
'''
使用圆括号把参数在URL模式里标识 出来  视 图函数就可以处理
'''
#引用当前目录的views
from . import views
urlpatterns = [
    path('hello',views.hello),
    path('login',views.login),
    path('test/<int:id>',views.test),
    path('search',views.search)

]