from django.shortcuts import render
from django.http import HttpResponse
import datetime
from .form import LoginForm
from .models import User
from django.shortcuts import redirect
from django.core.exceptions import ValidationError
from . import models
# Create your views here.
def current_datetime(request):
    now =datetime.datetime.now()
    html = '<html><body>It is now %s </body></html>'%now
    return HttpResponse(html)
#定义hello视图函数
def hello(request):
    print('调用index页面')
    return render(request,'index.html')
#登录
def login(request):
    print(request.path)
    print(request.method)
    # print(request.get_host())
    # print(request.get_full_path())
    # print(request.is_secure())
    # print(request.META)
    if request.method == 'POST':
        print('进入post')
        form = LoginForm(request.POST)
        if form.is_valid():
            print('数据正确')
            data = form.cleaned_data
            print(data)

            try:
                u1 = User.objects.get(uname=data['name'], upwd=data['pwd'])
            except User.DoesNotExist:
                print('异常报告:查不到')
                return render(request, 'login/login.html', {'form': form})

            if u1 != '' and u1 != None:
                print('进入判断之中')
                resp = HttpResponse()
                request.session['name'] = data['name']

                return redirect('/record/main')
            else:
                form = LoginForm()
                return render(request,'login/login.html',{'form':form})
        else:
            print(form.errors)
    else:
        print('进入GET')
        form = LoginForm()

    return render(request,'login/login.html',{'form':form})



#测试
def test(request,id):
    print(id)
    list = [2,3,5,6,7,8]
    print('进入测试页面')
    return HttpResponse({'list':list})

def search(request):
    print('表单上传数据')
    if 'q' in request.GET:
        message = 'You searched for: %r' % request.GET['q']
    else:
        message = 'You submitted an empty form.'
    return HttpResponse(message)
