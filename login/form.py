from django import forms
from django.core.exceptions import ValidationError
from . import models
class LoginForm(forms.Form):
    #name = forms.CharField(min_length=5,label='姓名',error_messages={'min_length':'短了','required':'字段不能为空'})
    name = forms.CharField(label='姓名',max_length=10,widget=forms.TextInput(attrs={'class':'form-control','style':'margin-bottom:2px'}))
    pwd = forms.CharField(label='密码',widget=forms.PasswordInput(attrs={'class':'form-control','style':'margin-bottom:2px'}),min_length=6)

    '''
    Django的form系统自动寻找匹配的函数方法，该方法名称以clean_开头，并以字段名称结束。 如果有这样的 方法，它将在校验时被调用。 
    '''
    def clean_name(self):
        name = self.cleaned_data['name']
        return name

    def clean_pwd(self):
        pwd = self.cleaned_data['pwd']
        return pwd