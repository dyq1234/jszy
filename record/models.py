from django.db import models
from login.models import User
# Create your models here.
class Record(models.Model):
    # sname = models.CharField(max_length=20)
    # sid = models.CharField(max_length=10)
    # ishy = models.BooleanField(default=False)

    phone = models.CharField(max_length=11)#号码
    description = models.TextField(null=True)#描述
    date = models.DateTimeField()#时间
    device = models.CharField(max_length=20,null=True)
    version = models.CharField(max_length=10)#版本
    solution = models.TextField(null=True)#解决方案
    comment = models.TextField(null=True)#备注
    state = models.NullBooleanField(default=False)#状态是否解决
    cdId = models.CharField(max_length=6,null=True)#禅道编号
    updateTime = models.DateField(auto_now=True,null=True)#修改时间
    update = models.ForeignKey(User,on_delete=models.CASCADE,related_name='update_user')#修改人
    createTime = models.DateField(auto_now_add=True)#创建时间
    creater = models.ForeignKey(User,on_delete=models.CASCADE,related_name='creater_user')#创建人

    class Meta:
        db_table='record'