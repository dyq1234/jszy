from django.shortcuts import render
from .models import Record
from django.shortcuts import redirect
from .form import RecordForm
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from datetime import datetime
from login.models import User
# Create your views here.
from django.http import HttpResponse

def reourd_show(request):
    print('进入main页面进行测试')
    if request.session.get('name') == None :
        print('session 为空')
        return redirect('/login/login')
    data = record_get()
    print(type(data))
    name = request.session.get('name')
    print('会话：'+name)
    if request.method =='POST':
        print('main页面进入post')
    else:
        print('main页面进入get')
        form = RecordForm()
    return render(request,'record/main.html',{'data':data})

#暂时不用
@csrf_exempt
def record_id_show(request):
    if request.method == 'POST':
        print('根据id查询问题，在模态框显示')
        id = request.POST['id']
    record = record_get_id(id)
    print('手机号码：'+record.phone)
    record_show = {'phone',record.phone}
    return JsonResponse(record_show,safe=False)

#显示所有学生
def record_get():
    data = Record.objects.all()
    return data
#根据id查询学生
def record_get_id(id):
    data = Record.objects.get(pk=id)
    return data

def update(request):
    if request.method == 'POST':
        print('进入POST 修改信息界面')
    else:
        print('进入GET 修改信息界面')
        id = request.GET['id']
        data = record_get_id(id)
    return render(request,'record/update.html',{'data':data})
#修改数据
def submit(request):

    if request.method == 'POST':
        print('数据保存')
        id = request.GET.get('id')
        phone = request.POST.get('phone')
        description = request.POST.get('description')
        device = request.POST.get('device')
        version = request.POST.get('version')
        solution = request.POST.get('solution')
        comment = request.POST.get('comment')
        state = request.POST.get('state')
        cdId = request.POST.get('cdId')
        Record.objects.filter(id = id).update(phone = phone,
                                              description = description,
                                              device = device,
                                              version = version,
                                              solution = solution,
                                              comment = comment,
                                              state = state,
                                              cdId = cdId)

    return redirect('/record/main')

#删除数据
def delete(request):
    id = request.GET.get('id')
    Record.objects.filter(id = id).delete()
    return redirect('/record/main')

#根据号码查询
def searchByPhone(request):
    if request.method =='POST':
        print('根据号码查询')
        phone = request.POST.get('phone')
        print(phone)
        data= Record.objects.filter(phone=phone)
        if len(data) ==0:
            return redirect('/record/main')

    return render(request,'record/main.html',{'data':data})

#进入新增页面
def add(request):
    return render(request,'record/add.html')
#新增问题记录
def addRecord(request):
    print('新增问题记录')
    phone = request.POST.get('phone')
    description = request.POST.get('description')
    device = request.POST.get('device')
    version = request.POST.get('version')
    solution = request.POST.get('solution')
    comment = request.POST.get('comment')
    state = request.POST.get('state')
    cdId = request.POST.get('cdId')
    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    createdate = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    updatedate = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    name = request.session.get('name')
    user = User.objects.filter(uname=name)
    id = user[0].id
    creater_id = id
    updater_id = id
    Record.objects.create(phone=phone,
                          description=description,
                          date=date,
                          device=device,
                          version=version,
                          solution=solution,
                          comment=comment,
                          state=state,
                          cdId=cdId,
                          updateTime=updatedate,
                          createTime=createdate,
                          creater_id=creater_id,
                          update_id=updater_id)
    return redirect('/record/main')