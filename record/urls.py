from django.urls import path
from . import views


urlpatterns = [
    path('main',views.reourd_show),
    path('main<int:id>',views.record_id_show),
    path('recordForm',views.record_id_show),
    path('update',views.update),
    path('submit',views.submit),
    path('delete',views.delete),
    path('searchByPhone',views.searchByPhone),
    path('add',views.add),
    path('addRecord',views.addRecord)

]