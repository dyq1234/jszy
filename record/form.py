from django import forms

class RecordForm(forms.Form):
    '''
    用户修改问题记录信息，可修改内容为：
    phone description device version solution comment state cdId
    '''

    phone = forms.CharField(max_length=11,widget=forms.TextInput(attrs={'class':'form-control'}))
    description = forms.CharField( max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
    device = forms.CharField( max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
    version = forms.CharField(max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
    solution = forms.CharField(max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
    comment = forms.CharField( max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
    state = forms.CharField(max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
    cdId = forms.CharField( max_length=11, widget=forms.TextInput(attrs={'class': 'form-control'}))
