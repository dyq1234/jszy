from django.contrib import admin
from .models import Record

# Register your models here.
@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    # 列表页属性
    list_display = ['phone',
                    'description',
                    'date',
                    'version',
                    'solution',
                    'comment',
                    'state']
    list_filter = ['phone',
                   'date']
    search_fields = ['phone']
    list_per_page = 10
